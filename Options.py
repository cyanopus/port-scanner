import argparse


class Options:

    def __init__(self):
        self.__parser = argparse.ArgumentParser()
        self.__args = {}

    def load_options(self):
        """
        Loads the options to the parser
        :return: None
        """
        self.__parser.add_argument('-n', '--network',
                                   type=str,
                                   nargs=1,
                                   required=True,
                                   help="Specify the network subnet in IP/CIDR format."
                                   )
        self.__parser.add_argument('-p', '--port',
                                   type=int,
                                   nargs=1,
                                   required=True,
                                   help="Specify the port number."
                                   )
        self.__parser.add_argument('-o', '--out',
                                   type=str,
                                   nargs=1,
                                   required=True,
                                   help="Specify the output file."
                                   )

    def load_options_from_file(self):
        """
        TODO: Loads the arguments from a file
        :return: None
        """
        pass

    def parse(self):
        """
        Parses the input arguments and converts them a dict of lists
        :return: None
        """
        self.__args = vars(self.__parser.parse_args())

    def get_args(self):
        return self.__args
