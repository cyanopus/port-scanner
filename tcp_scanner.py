#!/bin/env python3

from Options import Options
from TCPScanner import TCPScanner


if __name__ == '__main__':
    parser = Options()
    parser.load_options()
    parser.parse()

    network = parser.get_args()['network'][0]
    port = parser.get_args()['port'][0]
    out = parser.get_args()['out'][0]

    print("Starting port scanner on subnet {} for port {} ...".format(network, port))

    tcp_scanner = TCPScanner(network, port, out)
    tcp_scanner.scan_all_hosts_serial()

    print("Writing to a file - {}".format(out))

    tcp_scanner.write_to_file()
