import socket


class TCPConnection(object):

    def __init__(self, sfamily=socket.AF_INET, stype=socket.SOCK_STREAM):
        self.__s = socket.socket(sfamily, stype)
        self.__s.settimeout(1)

    def probe(self, host, port):
        """
        Connects to the host, closes the connection and returns True or False
        :param host: string of IP
        :param port: int Port
        :return: True or False
        """

        error_code = self.__s.connect_ex((host, port))
        self.__s.close()

        return error_code

    def __del__(self):
        self.__s.close()
