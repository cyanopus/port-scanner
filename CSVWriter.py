import csv
from FileWriter import FileWriter


class CSVWriter(FileWriter):

    def __init__(self, file):

        self.__file = file
        self.__writer = None
        self.__reader = None

    def write(self, data=None):

        if data is None:
            raise ValueError('Please, provide data to be written!')

        with open(self.__file, 'w') as csvfile:
            self.__writer = csv.writer(csvfile)

            for k, v in data.items():
                self.__writer.writerow([k, v])

    def read(self):

        with open(self.__file, 'rb') as csvfile:
            self.__reader = csv.reader(csvfile)
            for row in self.__reader:
                print(', '.join(row))
