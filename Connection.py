import abc


class Connection(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def probe(self):
        pass