import abc
import socket
from TCPConnection import TCPConnection


class ConnectionFactory(metaclass=abc.ABCMeta):

    @staticmethod
    def factory(type):
        if type is None:
            raise ValueError('Please enter a correct value!')
        if type.upper() == "TCP":
            return TCPConnection(stype=socket.SOCK_STREAM)
