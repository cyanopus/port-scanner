import abc


class Scanner(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def _scan(self):
        pass

    @abc.abstractmethod
    def write_to_file(self, file_format):
        pass

