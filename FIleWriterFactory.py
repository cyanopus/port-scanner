import abc
from CSVWriter import CSVWriter


class FileWriterFactory(metaclass=abc.ABCMeta):

    @staticmethod
    def factory(type, file):
        if type is None:
            raise ValueError('Please enter a correct type!')
        if type == "CSV":
            return CSVWriter(file)
