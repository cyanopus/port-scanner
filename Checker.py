import os
import ipaddress


class Checker(object):

    @staticmethod
    def is_port(value):
        return type(value) is int and \
               0 <= value <= 65535

    @staticmethod
    def is_file(value):
        return os.path.isfile(value)

    @staticmethod
    def is_host(value):
        return ipaddress.ip_address(value).is_global or \
               ipaddress.ip_address(value).is_private

    @staticmethod
    def is_network(value):
        return ipaddress.ip_network(value).is_global or \
               ipaddress.ip_network(value).is_private
