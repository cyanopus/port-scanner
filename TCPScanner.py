#!/bin/env python3

import ipaddress
from Checker import Checker
from Scanner import Scanner
from ConnectionFactory import ConnectionFactory
from FIleWriterFactory import FileWriterFactory


class TCPScanner(Scanner):

    def __init__(self, network=None, port=None, file=None):

        try:
            if not network and not port and not file:
                raise ValueError("Missing arguments.")
            if not Checker.is_network(network):
                raise ValueError("Not a correct network definition.")
            if not Checker.is_port(port):
                raise ValueError("Not a correct port definition. Must be between 0-65535.")
        except Exception as e:
            print(e)
            exit(1)

        self.__hosts = ipaddress.ip_network(network).hosts()
        self.__port = port
        self.__file = file
        self.__data = {'IP': 'Status'}
        self.__writer = None

    def _scan(self, host):
        """
        Probe a host for open TCP port given to the __init__.
        :param host: IP address
        :return: None
        """
        host = str(host)
        host_check = ConnectionFactory.factory('TCP')
        result_code = host_check.probe(host, self.__port)
        if not result_code:
            self.__data[host] = 'success'
        else:
            self.__data[host] = 'failure'

    def scan_all_hosts_serial(self):
        """
        Probe all the hosts in the provided network range.
        :return: None
        """

        for host in self.__hosts:
            print(host)
            self._scan(host)

    def async_scan(self):
        """
        TODO: Asynchronous scan function.
        :return: None
        """
        pass

    def write_to_file(self, file_format='CSV'):
        """
        Write to a file for provided file_format.
        :param file_format: the file format to be written.(CSV currently)
        :return: None
        """
        if file_format is None:
            file_format = 'CSV'
        self.__writer = FileWriterFactory.factory(file_format.upper(),
                                                  self.__file)
        self.__writer.write(self.__data)

    def get_hosts(self):

        return self.__hosts

    def get_port(self):

        return self.__port
